FROM node:alpine

WORKDIR /usr/src/app

COPY package.json package.json
RUN npm install sails
RUN npm install

COPY . .
# Wait for MySQL
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

EXPOSE 1337
CMD /wait && node ./app.js