#!/usr/bin/env python
"""
Takes a aws task definition as json, a new container image name
and print the new container definition.
"""

import sys
import json

def main():
  if len(sys.argv) is not 3:
    print >>sys.stderr, \
    'Expected 2 arguments. taskDefinition, containerImageName'
    exit(1)
  new_def = json.loads(sys.argv[1])['taskDefinition']
  new_def['containerDefinitions'][1]['image'] = sys.argv[2]
  new_def.pop('compatibilities', None)
  new_def.pop('requiresAttributes', None)
  new_def.pop('revision', None)
  new_def.pop('status', None)
  new_def.pop('taskDefinitionArn', None)
  print json.dumps(new_def)

if __name__ == '__main__':
  main()
